package com.bank.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {
	@Id
	@Column(length = 50)
	private String userid;
	@Column(length = 50)
	private String uname;
	@Column(length = 20)
	private String pwd;
	private int cid;
	public Admin() {
		// TODO Auto-generated constructor stub
	}
	
	public Admin(String userid, String uname, String pwd, int cid) {
		this.userid = userid;
		this.uname = uname;
		this.pwd = pwd;
		this.cid = cid;
	}

	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	
	
}
