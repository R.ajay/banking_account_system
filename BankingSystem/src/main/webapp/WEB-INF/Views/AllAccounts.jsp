<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h3 class="text-center m-2 border-bottom p-2">All Accounts List</h3>
<table class="table table-bordered table-sm table-striped">
    <thead class="table-dark">
        <tr>
            <th>Customer ID</th>
            <th>Customer Name</th>
            <th>Account Number</th>
            <th>Account Type</th>
            <th>Available Balance</th>    
            <th>Active</th>
        </tr>
    </thead>
    <tbody>
    <c:forEach items="${list }" var="acc">
        <tr>        
            <td>${acc.cid }</td>
            <td>${acc.customer.cname }</td>
            <td>${acc.accno }</td>
            <td>${acc.actype }</td>
            <td>&#8377; ${acc.bal }</td> 
            <td>${acc.isactive }</td>
        </tr>
       </c:forEach>
    </tbody>
</table>
<jsp:include page="footer.jsp" />