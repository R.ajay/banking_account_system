<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="text-center m-2 border-bottom p-2">Balance Enquiry</h3>
<jsp:include page="searchsection.jsp" />
<div class="row">
	<div class="col-8 mx-auto">
		<c:if test="${param.accno ne null}">
			<c:choose>
				<c:when test="${found }">
					<table class="table table-bordered">
						<tr>
							<th>Account Number</th>
							<th>${account.accno }</th>
						</tr>
						<tr>
							<th>Customer Name</th>
							<th>${account.customer.cname }</th>
						</tr>
						<tr>
							<th>Gender</th>
							<th>${account.customer.gender }</th>
						</tr>
						<tr>
							<th>Account Type</th>
							<th>${account.actype }</th>
						</tr>
						<tr class="table-success">
							<th>Balance Available</th>
							<th>&#8377; ${account.bal }</th>
						</tr>
					</table>
				</c:when>
				<c:otherwise>
					<div class="alert alert-danger text-center">
						<strong>Invalid Account Number</strong>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>


	</div>
</div>
<jsp:include page="footer.jsp" />