package com.bank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dao.BeneficiaryDAO;
import com.bank.model.Beneficiary;

@Service
public class BeneficiaryService {
	@Autowired BeneficiaryDAO dao;
	
	public List<Beneficiary> findByCustomerId(int cid){
		return dao.findByCustomerId(cid);
	}
	
	public void saveBeneficiary(Beneficiary b) {
		dao.save(b);
	}
}
