<jsp:include page="nbheader.jsp" />
 
<h4 class="text-center m-2">Welcome ${sessionScope.uname }</h4>

<h4 class="text-right">Current Available Balance : &#8377; ${account.bal }</h4>

<jsp:include page="footer.jsp" />