package com.bank.service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dao.AccountDAO;
import com.bank.dao.TransactionDAO;
import com.bank.model.Account;
import com.bank.model.Transactions;

@Service
public class TransactionService {
	@Autowired TransactionDAO dao;
	@Autowired AccountDAO adao;
	
	public void saveEntry(Transactions tran) {		
		tran.setAccount(adao.findById(tran.getAccno()).get());
		dao.save(tran);
	}
	
	public void deposit(Transactions tran) {
		tran.setTtype("Cash deposit");
		Account account=adao.findById(tran.getAccno()).get();
		tran.setAccount(account);
		dao.save(tran);
		account.setBal(account.getBal()+tran.getCramount());
		adao.save(account);
	}
	
	public void withdraw(Transactions tran) {		
		Account account=adao.findById(tran.getAccno()).get();
		tran.setAccount(account);
		dao.save(tran);
		account.setBal(account.getBal()-tran.getDramount());
		adao.save(account);
	}
	
	public void transfer(Transactions tran,int recaccno) {
		
		Transactions t1=new Transactions();
		Transactions t2=new Transactions();
		t1.setTdate(tran.getTdate());
		t1.setAccno(tran.getAccno());
		t1.setDramount(tran.getDramount());
		t1.setTtype("Cash Transferred to "+recaccno);
		Account account=adao.findById(t1.getAccno()).get();
		t1.setAccount(account);
		dao.save(t1);
		account.setBal(account.getBal()-t1.getDramount());
		adao.save(account);
		
		t2.setTdate(tran.getTdate());
		t2.setCramount(tran.getDramount());
		t2.setTtype("Cash Transferred from "+tran.getAccno());
		Account account2=adao.findById(recaccno).get();
		t2.setAccno(recaccno);
		t2.setAccount(account2);
		dao.save(t2);
		account2.setBal(account2.getBal()+t2.getCramount());
		adao.save(account2);
		System.out.println(t1);
		System.out.println(t2);
	}
	
	public List<Transactions> allTransactions(){
		return dao.findAll().stream()
				.sorted(Comparator.comparingInt(Transactions::getId).reversed())
				.collect(Collectors.toList());
	}
	
	public List<Transactions> allAccountTransactions(int accno){
		System.out.println(accno);
		return dao.findByAccno(accno).stream()
				.sorted(Comparator.comparingInt(Transactions::getId).reversed())
				.collect(Collectors.toList());
	}
	
	public List<Transactions> todaysTransactions(){
		return dao.findAll().stream().filter(t->t.getTdate().toLocalDate().equals(LocalDate.now())).collect(Collectors.toList());
	}
	
	public int todaysDebits(){
		return dao.todaysDebit()==null ? 0 : dao.todaysDebit();
	}
	
	public int todaysCredits(){
		return dao.todaysCredit()==null ? 0 : dao.todaysCredit();
	}
}
