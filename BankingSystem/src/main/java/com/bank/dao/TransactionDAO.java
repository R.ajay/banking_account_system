package com.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.model.Transactions;

@Repository
public interface TransactionDAO extends JpaRepository<Transactions, Integer> {

	@Query("SELECT t FROM Transactions t WHERE t.accno=?1")
	List<Transactions> findByAccno(int accno);
	
	@Query("SELECT sum(dramount) from Transactions where tdate=date(now())")
	Integer todaysDebit();
	
	@Query("SELECT sum(cramount) from Transactions where tdate=date(now())")
	Integer todaysCredit();
}
