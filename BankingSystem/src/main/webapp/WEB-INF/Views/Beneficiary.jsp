<jsp:include page="nbheader.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h3 class="text-center border-bottom border-primary p-2">Beneficiary</h3>
<jsp:include page="message.jsp" />
<div class="row">
    <div class="col-sm-8">        
        <h4 class="text-center p-2">All Beneficiary</h4>
        <table class="table table-bordered table-sm">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Beneficiary Name</th>
                    <th>Bank Name</th>
                    <th>Account Number</th>
                    <th>Max Limit</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${list }" var="b">
                <tr>
                    <td>${b.bid }</td>
                    <td>${b.bname }</td>
                    <td>${b.bankname }</td>
                    <td>${b.accno }</td>
                    <td>&#8377; ${b.maxlimit }</td>
                </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="col-sm-4">
        <h4 class="text-center p-2">Add Beneficiary</h4>
        <form method="post">
            <input type="hidden" name="cid" value="${sessionScope.cid}">
            <div class="form-group">
                <input type="text" required name="bname" placeholder="Beneficiary Name" 
                       class="form-control">
            </div>
            <div class="form-group">
                <input type="text" required name="bankname" placeholder="Bank Name" 
                       class="form-control">
            </div>
            <div class="form-group">
                <input type="text" required name="IFSC" placeholder="Bank IFSC Code" 
                       class="form-control">
            </div>
            <div class="form-group">
                <input type="text" required name="accno" placeholder="Account Number" 
                       class="form-control">
            </div>
            <div class="form-group">
                <input type="number" required name="maxlimit" placeholder="Maximum Limit" 
                       class="form-control">
            </div>
            <input type="submit" value="Add Beneficiary" class="btn btn-primary float-right">
        </form>
    </div>    
</div>

<jsp:include page="footer.jsp" />