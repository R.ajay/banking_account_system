<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h3 class="text-center">Open Customers Account</h3>
<div class="row">
    <div class="col-5 mx-auto">
        <jsp:include page="message.jsp" />
        <div class="card p-3">
            <form method="post">
                <div class="form-group">      
                    <label>Select Customer</label>
                    <select name="cid" required class="form-control">
                        <option value="">--- Select Customer ---</option>
                        	<c:forEach items="${list }" var="cust">
                        		<option value="${cust.id }">${cust.cname }</option>
                        	</c:forEach>                      
                    </select>
                </div>
                <div class="form-group">                
                    <label>Select Account Type</label>
                    <select name="actype" required class="form-control">
                        <option value="">--- Select Account Type ---</option>
                        <option>Saving Account</option>
                        <option>Current Account</option>
                        <option>Term Deposit Account</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Opening Amount</label>
                    <input type="text" name="bal" required
                           class="form-control" pattern="[0-9]{0,10}"
                           placeholder="Opening Amount" maxlength="10">
                </div>            
                <input type="submit" value="Open Account" class="btn btn-primary btn-block">
            </form> 
        </div>
    </div>
</div>

<jsp:include page="footer.jsp" />