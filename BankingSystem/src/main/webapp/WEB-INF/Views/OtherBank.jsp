<jsp:include page="nbheader.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="text-center border-bottom border-primary p-2">Other Bank
	Transfer</h3>
<jsp:include page="message.jsp" />
<div class="row">
	<div class="col-sm-6 mx-auto">
		<form method="post">
			<input type="hidden" name="accno" value="${account.accno }">
			<div class="form-group">
				<label>Select Beneficiary</label> <select type="text" name="bname"
					class="form-control">
					<option>---- Select Beneficiary ---</option>
					<c:forEach items="${list }" var="b">
						<option>${b.bname }</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group">
				<label> Remarks </label> <input type="text" name="ttype"
					class="form-control">
			</div>
			<div class="form-group">
				<label>Available Balance</label> <input type="text"
					value="${account.bal }" readonly class="form-control">
			</div>
			<div class="form-group">
				<label> Transfer Amount</label> <input type="number" min="1"
					name="dramount" class="form-control">
			</div>
			<button id="transfer" class="btn btn-primary float-right">Transfer
				Now</button>
		</form>
	</div>
</div>
<jsp:include page="footer.jsp" />