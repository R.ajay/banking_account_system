package com.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.model.Customer;

@Repository
public interface CustomerDAO extends JpaRepository<Customer, Integer> {

	@Query("SELECT c FROM Customer c WHERE c.id not in (SELECT a.cid FROM Account a)")
	List<Customer> findPendingCustomer();
}
