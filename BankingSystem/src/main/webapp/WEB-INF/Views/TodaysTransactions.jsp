<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="text-center m-2 border-bottom border-primary p-2">Today's
	Transactions Report</h3>

<table class="table table-bordered table-striped table-sm">
	<thead class="table-primary">
		<tr>
			<td>Account Number</td>
			<td>Customer Name</td>
			<td>Particulars</td>
			<td>Dr Amount</td>
			<td>Cr Amount</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${trans }" var="tran">
			<tr>
				<td>${tran.accno }</td>
				<td>${tran.account.customer.cname }</td>
				<td>${tran.ttype }</td>
				<td>${tran.dramount==0 ? "" : tran.dramount}</td>
				<td>${tran.cramount==0 ? "":tran.cramount }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<jsp:include page="footer.jsp" />