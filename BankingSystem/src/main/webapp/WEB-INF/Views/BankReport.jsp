<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h4>Bank Transaction Report</h4>
<table id="tbl" class="table table-bordered table-striped">
    <thead class="table-primary">
        <tr>
            <th>Transaction Date</th>
            <th>Account Number</th>
            <th>Customer Name</th>
            <th>Particulars</th>
            <th>Dr Amount</th>
            <th>Cr Amount</th>                         
        </tr>
    </thead>
    <tbody>
        <tr>
           <c:forEach items="${trans }" var="tran">
			<tr>
				<td>${tran.tdate }</td>
				<td>${tran.accno }</td>
				<td>${tran.account.customer.cname }</td>
				<td>${tran.ttype }</td>
				<td>${tran.dramount==0 ? "" : tran.dramount}</td>
				<td>${tran.cramount==0 ? "":tran.cramount }</td>
			</tr>
		</c:forEach>
        </tr>
    </tbody>
</table>
<script>
$(function(){
    $("#tbl").dataTable({
        ordering:false               
    });    
});
</script>
<jsp:include page="footer.jsp" />