package com.bank.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String cname;
	private String address;
	private String gender;
	private String dob;
	private String adhar;
	private String phone;
	private String email;
	private String pic;
	private String signature;
	@OneToMany
	@JoinColumn(name="accno")
	private Set<Account> accounts;
	@OneToMany
	@JoinColumn(name="cid")
	private Set<Beneficiary> beneficiary;
	public Customer() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Customer(String cname, String address, String gender, String dob, String adhar, String phone,
			String email, String pic, String signature) {
		this.cname = cname;
		this.address = address;
		this.gender = gender;
		this.dob = dob;
		this.adhar = adhar;
		this.phone = phone;
		this.email = email;
		this.pic = pic;
		this.signature = signature;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAdhar() {
		return adhar;
	}
	public void setAdhar(String adhar) {
		this.adhar = adhar;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}


	@Override
	public String toString() {
		return "Customer [id=" + id + ", cname=" + cname + ", address=" + address + ", gender=" + gender + ", dob="
				+ dob + ", adhar=" + adhar + ", phone=" + phone + ", email=" + email + "]";
	}
	
	
}
