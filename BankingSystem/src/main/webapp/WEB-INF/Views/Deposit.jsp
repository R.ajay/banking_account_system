<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h3 class="text-center m-2 border-bottom p-2">Deposit Module</h3>
<jsp:include page="message.jsp" />
<jsp:include page="searchsection.jsp" />
<div class="row">
    <div class="col-8 mx-auto">
    <c:if test="${param.accno ne null}">
    	<c:choose>
    		<c:when test="${found }">           
		        <table class="table table-bordered">
		            <tr>
		                <th>Account Number</th>
		                <th>${account.accno }</th>
		                <th>Customer Name</th>
		                <th>${account.customer.cname }</th>
		            </tr>
		            <tr class="table-success">
		                <th>Account Type</th>
		                <th>${account.actype }</th>
		                <th>Balance Available</th>
		                <th>&#8377; ${account.bal }</th>
		            </tr>            
		        </table>
		            <form method="post" action="/deposit">
		            <input type="hidden" name="accno" value="${param.accno }" >
		            <div class="row">
		                <div class="col-6">
		                    <div class="form-group">
		                        <label>Transaction Date</label>
		                        <input type="date" id="tdate" name="tdate" class="form-control">
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="form-group">
		                        <label>Deposit Amount</label>
		                        <input type="number" min="1" name="cramount" class="form-control">
		                    </div>
		                </div>
		            </div>
		            <button class="btn btn-primary float-right">Deposit</button>
		        </form>
        </c:when>
        <c:otherwise>
        <div class="alert alert-danger text-center">
            <strong>Invalid Account Number</strong>
        </div>
        </c:otherwise>
        </c:choose>
        </c:if>     
    </div>
</div>
<script>
    document.getElementById("tdate").valueAsDate=new Date();
</script>
<jsp:include page="footer.jsp" />