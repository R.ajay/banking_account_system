<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h3 class="text-center m-2">All Customers</h3>
<table class="table table-bordered table-sm">
    <thead class="table-dark">
        <tr>
            <th>Customer ID</th>
            <th>Customer Name</th>
            <th>Address</th>
            <th>Gender</th>
            <th>Adhar ID</th>
            <th>Phone No.</th>
            <th>Email Address</th>            
        </tr>
    </thead>
    <tbody>
    <c:forEach items="${list }" var="cust">
        <tr>
            <td>${cust.id }</td>
            <td>${cust.cname }</td>
            <td>${cust.address }</td>
            <td>${cust.gender }</td>
            <td style="letter-spacing: 0.1em;">${cust.adhar }</td>
            <td>${cust.phone }</td>
            <td>${cust.email }</td>            
        </tr>
       </c:forEach>
    </tbody>
</table>
<jsp:include page="footer.jsp" />