<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${sessionScope.msg ne null}">
<div class="alert alert-success text-center">
    <strong>${sessionScope.msg }</strong>
</div>
<c:remove var="msg" scope="session"/>
</c:if>

<c:if test="${sessionScope.error ne null}">
<div class="alert alert-danger text-center">
    <strong>${sessionScope.error }</strong>
</div>
<c:remove var="error" scope="session"/>
</c:if>
