package com.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.model.Admin;

@Repository
public interface AdminDAO extends JpaRepository<Admin, String> {


}
