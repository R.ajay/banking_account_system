<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/bank.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="body">
        <div class="jumbotron bg-transparent p-4 text-center text-white border-bottom">
            <h1>Welcome to Cognizant International Bank</h1>    
        </div>
        <div class="container">
            <div class="row">
                <div class="col-9 mx-auto text-center">
                    <a href="login">
                    <div class="d-inline-block border p-2" style="width:240px;">
                        <img src="images/bank.jpg" class="img-thumbnail" style="height:200px;width:200px;">
                        <h3 class="text-white">Admin</h3>
                    </div>
                    </a>
                    <a href="register">`
                    <div class="d-inline-block border p-2" style="width:240px">
                        <img src="images/customer.png" class="img-thumbnail" style="height:200px;width:200px;">
                        <h3 class="text-white">New Customer</h3>
                    </div>
                    </a>
                    <a href="netbanking">`
                    <div class="d-inline-block border p-2" style="width:240px">
                        <img src="images/nbanking.jpg" class="img-thumbnail" style="height:200px;width:200px;">
                        <h3 class="text-white">User</h3>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </body>
</html>
