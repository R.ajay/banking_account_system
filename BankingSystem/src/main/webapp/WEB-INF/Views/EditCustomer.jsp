<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="text-center m-2 border-bottom p-2">Update Customer
	Information</h3>
<jsp:include page="searchsection.jsp" />
<div class="row">
	<div class="col-10 mx-auto">
		<jsp:include page="message.jsp" />
		<c:if test="${param.accno ne null}">
			<c:choose>
				<c:when test="${found }">
					<form action="/rekyc" method="post">
						<input type="hidden" name="id" value="${account.cid }">
						<table class="table table-bordered border-primary">
							<tr>
								<th>Account Number</th>
								<th>${account.accno }</th>
								<th>Customer Name</th>
								<th>${account.customer.cname }</th>
							</tr>
							<tr class="table-success">
								<th>Account Type</th>
								<th>${account.actype }</th>
								<th>Balance Available</th>
								<th>&#8377; ${account.bal }</th>
							</tr>
							<tr>
								<th>Customer Photo</th>
								<th><<img src="cpics/${account.customer.pic }"
										style="height: 100px;" class="img-thumbnail">
								</th>
								<th>Customer Signature</th>
								<th><img src="csign/${account.customer.signature }"
										style="height: 100px;" class="img-thumbnail">
								</th>
							</tr>
							<tr>
								<th>Customer Name</th>
								<th><input type="text" class="form-control" name="cname"
									value="${account.customer.cname }"></th>
								<th>Customer Address</th>
								<th><input type="text" class="form-control" name="address"
									value="${account.customer.address }"></th>
							</tr>
							<tr>
								<th>Gender</th>
								<th><input type="text" class="form-control" name="gender"
									value="${account.customer.gender }"></th>
								<th>Phone Number</th>
								<th><input type="text" class="form-control" name="phone"
									value="${account.customer.phone }"></th>
							</tr>
							<tr>
								<th>ADHAR ID</th>
								<th><input type="text" class="form-control" name="adhar"
									value="${account.customer.adhar }"></th>
								<th>Email ID</th>
								<th><input type="email" class="form-control" name="email"
									value="${account.customer.email }"></th>
							</tr>
						</table>
						<input type="submit" value="Update Information"
							class="btn btn-primary float-right">
					</form>
				</c:when>
				<c:otherwise>
					<div class="alert alert-danger text-center">
						<strong>Invalid Account Number</strong>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>

	</div>
</div>
<jsp:include page="footer.jsp" />