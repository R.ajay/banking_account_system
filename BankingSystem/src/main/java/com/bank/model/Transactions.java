package com.bank.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Transactions {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private Date tdate;
	private String ttype;	
	private int accno;
	private float dramount;
	private float cramount;
	@ManyToOne
	private Account account;
	public Transactions() {
		// TODO Auto-generated constructor stub
	}
	public Transactions(Date tdate, String ttype, int accno, float dramount, float cramount) {
		this.tdate = tdate;
		this.ttype = ttype;
		this.accno = accno;
		this.dramount = dramount;
		this.cramount = cramount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTdate() {
		return tdate;
	}
	public void setTdate(Date tdate) {
		this.tdate = tdate;
	}
	public String getTtype() {
		return ttype;
	}
	public void setTtype(String ttype) {
		this.ttype = ttype;
	}
	public int getAccno() {
		return accno;
	}
	public void setAccno(int accno) {
		this.accno = accno;
	}
	public float getDramount() {
		return dramount;
	}
	public void setDramount(float dramount) {
		this.dramount = dramount;
	}
	public float getCramount() {
		return cramount;
	}
	public void setCramount(float cramount) {
		this.cramount = cramount;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	@Override
	public String toString() {
		return "Transactions [id=" + id + ", tdate=" + tdate + ", ttype=" + ttype + ", accno=" + accno + ", dramount="
				+ dramount + ", cramount=" + cramount + "]";
	}
	
	
}
