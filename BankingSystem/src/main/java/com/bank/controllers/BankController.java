package com.bank.controllers;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import com.bank.model.Account;
import com.bank.model.Admin;
import com.bank.model.Customer;
import com.bank.model.Transactions;
import com.bank.model.Users;
import com.bank.service.AccountService;
import com.bank.service.CustomerService;
import com.bank.service.TransactionService;
import com.bank.service.UserService;

@Controller
public class BankController {
	
	@Autowired UserService userservice;
	@Autowired CustomerService custservice;
	@Autowired TransactionService transervice;
	@Autowired HttpSession session;
	@Autowired AccountService accservice;
	
	@GetMapping(path ={"/","/home"})
	public String Home() {
		userservice.createAdmin();
		return "index";
	}
	
	@GetMapping("/login")
	public String LoginPage(){
		return "Login";
	}
	
	@GetMapping("/change")
	public String ChangePassword(){
		return "ChangePwd";
	}
	
	@PostMapping("/change")
	public String ChangePassword(Users user,String opwd){
		if(userservice.ValidatePwd(user,opwd)) {
			userservice.ChangePwd(user);
			session.setAttribute("msg", "Password updated");
		}else {
			session.setAttribute("error", "Invalid current password");
		}
		return "redirect:/change";
	}
	
	@GetMapping("/register")
	public String Register() {
		return "CustomerRegister";
	}
	
	@GetMapping("/todays")
	public String todayTransactions(Model model) {
		model.addAttribute("trans", transervice.todaysTransactions());
		return "TodaysTransactions";
	}
	
	@GetMapping("/bankreport")
	public String bankReport(Model model) {
		model.addAttribute("trans", transervice.allTransactions());
		return "BankReport";
	}
	
	
	@GetMapping("/deposit")
	public String Deposit(Optional<Integer> accno,Model model) {
		if(accno.isPresent()) {
			Account account=accservice.findByAccno(accno.get());
			if(account==null)
				model.addAttribute("found", false);
			else {
				model.addAttribute("found", true);
				model.addAttribute("account", account);
			}
		}
		return "Deposit";
	}
	
	@GetMapping("/transfer")
	public String Transfer(Optional<Integer> accno,Model model) {
		if(accno.isPresent()) {
			Account account=accservice.findByAccno(accno.get());
			if(account==null)
				model.addAttribute("found", false);
			else {
				model.addAttribute("found", true);
				model.addAttribute("account", account);
			}
		}
		return "TransferAmount";
	}
	
	@PostMapping("/transfer")
	public String Transfer(Transactions tran,int recaccno) {
		transervice.transfer(tran, recaccno);
		session.setAttribute("msg", "Transfer successfull");
		return "redirect:/transfer";
	}
	
	@GetMapping("/balenq")
	public String BalanceEnquiry(Optional<Integer> accno,Model model) {
		if(accno.isPresent()) {
			Account account=accservice.findByAccno(accno.get());
			if(account==null)
				model.addAttribute("found", false);
			else {
				model.addAttribute("found", true);
				model.addAttribute("account", account);
			}
		}
		return "BalanceEnq";
	}
	
	@GetMapping("/rekyc")
	public String ReKyc(Optional<Integer> accno,Model model) {
		if(accno.isPresent()) {
			Account account=accservice.findByAccno(accno.get());
			if(account==null)
				model.addAttribute("found", false);
			else {
				model.addAttribute("found", true);
				model.addAttribute("account", account);
			}
		}
		return "EditCustomer";
	}
	
	@PostMapping("/rekyc")
	public String Rekyc(Customer c) {	
		System.out.println(c);
		custservice.updateCustomer(c);
		session.setAttribute("msg", "Customer information updated");
		return "redirect:/rekyc";
	}
	
	@GetMapping("/close")
	public String CloseAccount(Optional<Integer> accno,Model model) {
		if(accno.isPresent()) {
			Account account=accservice.findByAccno(accno.get());
			if(account==null)
				model.addAttribute("found", false);
			else {
				model.addAttribute("found", true);
				model.addAttribute("account", account);
			}
		}
		return "CloseAccount";
	}	
	
	@PostMapping("/close")
	public String CloseAccount(int accno) {
		accservice.deactivate(accno);
		session.setAttribute("msg", "Account deactivated");
		return "redirect:/close";
	}
	
	@GetMapping("/reactivate")
	public String ReActivate(Optional<Integer> accno,Model model) {
		if(accno.isPresent()) {
			Account account=accservice.findDeactiateByAccno(accno.get());
			if(account==null)
				model.addAttribute("found", false);
			else {
				model.addAttribute("found", true);
				model.addAttribute("account", account);
			}
		}
		return "Reactivate";
	}
	
	@PostMapping("/reactivate")
	public String reactivateAccount(int accno) {
		accservice.activate(accno);
		session.setAttribute("msg", "Account activated");
		return "redirect:/close";
	}
	
	@GetMapping("/passbook")
	public String Passbook(Optional<Integer> accno,Model model) {
		if(accno.isPresent()) {
			Account account=accservice.findByAccno(accno.get());
			if(account==null)
				model.addAttribute("found", false);
			else {
				model.addAttribute("found", true);
				model.addAttribute("account", account);
				List<Transactions> tlist=account.getTransactions().stream()
						.sorted(Comparator.comparingInt(Transactions::getId).reversed())						
						.collect(Collectors.toList());
				System.out.println("This is"+tlist.size());
				model.addAttribute("trans",tlist );
			}
		}
		return "TransactionReport";
	}
	
	@PostMapping("/deposit")
	public String Deposit(Transactions tran) {		
		transervice.deposit(tran);
		session.setAttribute("msg", "Deposited successfully");
		return "redirect:/deposit";
	}
	
	@GetMapping("/withdraw")
	public String Withdrawal(Optional<Integer> accno,Model model) {
		if(accno.isPresent()) {
			Account account=accservice.findByAccno(accno.get());
			if(account==null)
				model.addAttribute("found", false);
			else {
				model.addAttribute("found", true);				
				model.addAttribute("account", account);
			}
		}
		return "Withdrawal";
	}
	
	@PostMapping("/withdraw")
	public String Withdraw(Transactions tran) {	
		tran.setTtype("Cash withdraw");
		transervice.withdraw(tran);
		session.setAttribute("msg", "Withdraw successfully");
		return "redirect:/withdraw";
	}
	
	@PostMapping("/RegisterProcess")
	public String RegisterProcess(MultipartFile photo,MultipartFile sign,Customer c) {
		custservice.addCustomer(c,photo,sign);
		session.setAttribute("msg", "Customer registered successfully");
		return "redirect:/register";
	}
	
	@GetMapping("/customers")
	public String listAllCustomers(Model model) {
		model.addAttribute("list",custservice.getAllCustomers());
		return "AllCustomers";
	}
	
	@GetMapping("/accounts")
	public String listAccounts(Model model) {
		model.addAttribute("list", accservice.allAccounts());
		return "AllAccounts";
	}
	
	@GetMapping("/open")
	public String openAccount(Model model) {
		List<Customer> list=custservice.getPendingCustomers();
		if(list.size()>0) {
			model.addAttribute("list",list );
		}
		return "OpenAccount";
	}
	
	@PostMapping("/open")
	public String saveAccount(Account acc,int bal) {
		accservice.openAccount(acc,bal);
		session.setAttribute("msg", "Account opened successfully");
		return "redirect:/open";
	}
	@PostMapping("/Validate")
	 public String Login(String userid,String pwd) {
	  Admin admin=userservice.Adminlogin(userid, pwd);
	  if(admin!=null) {
	   session.setAttribute("userid", userid);
	   session.setAttribute("uname", admin.getUname());
	   session.setAttribute("cid", admin.getCid());
	   if(userservice.ValidateAdminPwd(admin,pwd)) {
	    return  "redirect:/dashboard";
	   }else {
	    session.setAttribute("msg", "Invalid password");
	    return "redirect:/login";
	   }
	   //return  "redirect:/nbhome";
	  }else {
	   session.setAttribute("msg", "Invalid userid or pasword");
	   return "redirect:/login";
	  }
	 }
	/*
	@PostMapping("/Validate")
	public String Validate(String userid,String pwd) {
		Users user=userservice.login(userid, pwd);
		if(user==null)
			return "redirect:/login";
		else {
			session.setAttribute("userid", userid);
			return "redirect:/dashboard";
		}
	}
	*/
	
	@GetMapping("/dashboard")
	public String AdminDashboard(Model model) {
		model.addAttribute("actives", accservice.allAccounts().size());
		model.addAttribute("debit", transervice.todaysDebits());
		model.addAttribute("credit", transervice.todaysCredits());
		model.addAttribute("inhand", transervice.todaysCredits()-transervice.todaysDebits());
		return "/AdminHome";
	}
	
	@GetMapping("/Logout")
	public String Logout() {
		session.invalidate();
		return "redirect:/home";
	}
}
