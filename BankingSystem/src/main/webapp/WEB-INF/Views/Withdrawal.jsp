<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h3 class="text-center m-2 border-bottom p-2">Withdraw Module</h3>
<jsp:include page="message.jsp" />
<jsp:include page="searchsection.jsp" />
<div class="row">
    <div class="col-9 mx-auto"> 
    <c:if test="${param.accno ne null}">
    	<c:choose>
    		<c:when test="${found }">       
        <div class="row mb-2">
            <div class="col-sm-3 p-0">
                <img src="../cpics/${account.customer.pic }" class="img-thumbnail" style="width:100%;height:100%;">
            </div>
            <div class="col-sm-9">
                <table class="table table-bordered">
                    <tr>
		                <th>Account Number</th>
		                <th>${account.accno }</th>
		                <th>Customer Name</th>
		                <th>${account.customer.cname }</th>
		            </tr>
		            <tr class="table-success">
		                <th>Account Type</th>
		                <th>${account.actype }</th>
		                <th>Balance Available</th>
		                <th>&#8377; ${account.bal }</th>
		            </tr>   
                    <tr>
                        <th class="align-middle text-right" rowspan="2">Signature</th>
                        <th colspan="3"><img style="height:100px;width:200px;" src="../csign/${account.customer.signature }"></th>
                    </tr>
                </table>
            </div>
        </div>

        <form method="post" action="/withdraw">
            <input type="hidden" name="accno" value="${param.accno }" >
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Transaction Date</label>
                        <input type="date" id="tdate" name="tdate" class="form-control">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Withdraw Amount</label>
                        <input type="number" min="1" name="dramount" class="form-control">
                    </div>
                </div>
            </div>
            <button class="btn btn-primary float-right">Withdraw</button>
        </form>
	</c:when>
	</c:choose>
	</c:if>
    </div>
</div>
<script>
    document.getElementById("tdate").valueAsDate = new Date();
</script>
<jsp:include page="footer.jsp" />