<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="text-center m-2 border-bottom p-2">Transfer Amount</h3>
<jsp:include page="message.jsp" />
<jsp:include page="searchsection.jsp" />
<div class="row">
	<div class="col-8 mx-auto">
		<c:if test="${param.accno ne null}">
			<c:choose>
				<c:when test="${found }">
					<table class="table table-bordered">
						<tr>
							<th>Account Number</th>
							<th>${account.accno }</th>
							<th>Customer Name</th>
							<th>${account.customer.cname }</th>
						</tr>
						<tr class="table-success">
							<th>Account Type</th>
							<th>${account.actype }</th>
							<th>Balance Available</th>
							<th>&#8377; ${account.bal }</th>
						</tr>
					</table>
					<form method="post" action="transfer">
						<input type="hidden" name="accno"
							value="${param.accno }">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>Receiver Account Number</label> <input type="text"
										id="recaccno" name="recaccno" class="form-control">
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>Receiver Name</label> <input type="text" id="recname"
										readonly class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>Transaction Date</label> <input type="date" id="tdate"
										name="tdate" class="form-control">
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>Transfer Amount</label> <input type="number" min="1"
										name="dramount" class="form-control">
								</div>
							</div>
						</div>
						<button id="transfer" disabled class="btn btn-primary float-right">Transfer
							Now</button>
					</form>
				</c:when>
				<c:otherwise>
					<div class="alert alert-danger text-center">
						<strong>Invalid Account Number</strong>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
	</div>
</div>
<script>
    document.getElementById("tdate").valueAsDate = new Date();
    $("#recaccno").blur(function(){
       //alert(this.value); 
       $.post("/CheckName",{"accno":this.value},function(output){
          console.log(output);
           if(output.trim()==="no"){
               $("#transfer").attr("disabled","disabled");
               $("#recaccno").css({'border':'2px solid red'});
           }else{
               $("#transfer").removeAttr("disabled");  
               $("#recname").val(output); 
               $("#recaccno").css({'border':'2px solid green'});
           }
          
       });
    });
</script>
<jsp:include page="footer.jsp" />