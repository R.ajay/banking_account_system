package com.bank.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dao.AdminDAO;
import com.bank.dao.UserDAO;
import com.bank.model.Admin;
import com.bank.model.Users;

@Service
public class UserService {
	@Autowired UserDAO dao;
	@Autowired AdminDAO admindao;
	public void createAdmin() {
		if(dao.count()==0) {
			dao.save(new Users("admin","Administrator","admin",0));
		}
	}
	
	public void AddUser(Users user) {
		dao.save(user);
	}
	
	public void ChangePwd(Users user) {
		Users u=dao.findById(user.getUserid()).get();
		u.setPwd(user.getPwd());
		dao.save(user);
	}
	
	public boolean ValidatePwd(Users user,String opwd) {
		Users u=dao.findById(user.getUserid()).get();
		return u.getPwd().equals(opwd);					
	}
	
	public boolean ValidateAdminPwd(Admin user,String opwd) {
		Admin u=admindao.findById(user.getUserid()).get();
		return u.getPwd().equals(opwd);					
	}
	
	public Admin Adminlogin(String userid,String pwd) {
		Optional<Admin> user=admindao.findById(userid);
		if(user.isPresent()) {
			return user.get();
		}
		else {
			return null;
		}
	}
	
	public Users login(String userid,String pwd) {
		Optional<Users> user=dao.findById(userid);
		if(user.isPresent()) {
			return user.get();
		}
		else {
			return null;
		}
	}
	
	public List<Users> getAllUsers(){
		return dao.findAll();
	}
}
