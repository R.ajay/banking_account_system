package com.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.model.Beneficiary;

@Repository
public interface BeneficiaryDAO extends JpaRepository<Beneficiary, Integer> {

	@Query("SELECT b from Beneficiary b WHERE b.cid=?1")
	List<Beneficiary> findByCustomerId(int cid);
}
