<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="text-center m-2 border-bottom p-2">Transactions Report</h3>
<jsp:include page="searchsection.jsp" />
<div class="row">
	<div class="col-10 mx-auto">
		<c:if test="${param.accno ne null}">
			<c:choose>
				<c:when test="${found }">
					<table class="table table-bordered">
						<tr>
							<th>Account Number</th>
							<th>${account.accno }</th>
							<th>Customer Name</th>
							<th>${account.customer.cname }</th>
						</tr>
						<tr class="table-success">
							<th>Account Type</th>
							<th>${account.actype }</th>
							<th>Balance Available</th>
							<th>&#8377; ${account.bal }</th>
						</tr>
					</table>
					<table class="table table-bordered table-striped table-sm">
						<thead class="table-primary">
							<tr>
								<th>Id</th>
								<th>Transaction Date</th>
								<th>Particulars</th>
								<th>Dr Amount</th>
								<th>Cr Amount</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${trans }" var="tran">
							<tr>
								<td>${tran.id }</td>
								<td>${tran.tdate }</td>
								<td>${tran.ttype }</td>
								<td>${tran.dramount==0 ? "" : tran.dramount}</td>
								<td>${tran.cramount==0 ? "":tran.cramount }</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</c:when>
				<c:otherwise>
					<div class="alert alert-danger text-center">
						<strong>Invalid Account Number</strong>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>
	</div>
</div>
<jsp:include page="footer.jsp" />