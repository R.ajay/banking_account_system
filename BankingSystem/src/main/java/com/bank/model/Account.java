package com.bank.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Account {
	private String actype;
	@Id
	private int accno;
	private int cid;
	private String isactive;
	private float bal;
	@ManyToOne
	private Customer customer;
	@OneToMany
	@JoinColumn(name="accno")
	private Set<Transactions> transactions;
	public Account() {
		// TODO Auto-generated constructor stub
	}
	public Account(String actype, int accno, int cid, String isactive,float bal) {
		this.actype = actype;
		this.accno = accno;
		this.cid = cid;
		this.isactive = isactive;
		this.bal=bal;
	}

	public String getActype() {
		return actype;
	}
	public void setActype(String actype) {
		this.actype = actype;
	}
	public int getAccno() {
		return accno;
	}
	public void setAccno(int accno) {
		this.accno = accno;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getIsactive() {
		return isactive;
	}
	public void setIsactive(String isactive) {
		this.isactive = isactive;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Set<Transactions> getTransactions() {
		return transactions;
	}
	public void setTransactions(Set<Transactions> transactions) {
		this.transactions = transactions;
	}
	public float getBal() {
		return bal;
	}
	public void setBal(float bal) {
		this.bal = bal;
	}
	
	
}
