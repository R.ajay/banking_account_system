<jsp:include page="nbheader.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="text-center border-bottom border-primary p-2">Transactions Report</h3>
<table class="table table-bordered table-striped table-sm">
	<thead class="table-primary">
		<tr>
		<th>Id</th>
			<th>Transaction Date</th>
			<th>Particulars</th>
			<th>Dr Amount</th>
			<th>Cr Amount</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${trans }" var="tran">
			<tr>
			<td>${tran.id }</td>
				<td>${tran.tdate }</td>
				<td>${tran.ttype }</td>
				<td>${tran.dramount==0 ? "" : tran.dramount}</td>
				<td>${tran.cramount==0 ? "":tran.cramount }</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<jsp:include page="footer.jsp" />