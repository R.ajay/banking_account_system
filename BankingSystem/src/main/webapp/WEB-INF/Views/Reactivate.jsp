<jsp:include page="header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="text-center m-2 border-bottom p-2">Reactivate Account</h3>
<jsp:include page="message.jsp" />
<jsp:include page="searchsection.jsp" />
<div class="row">
	<div class="col-8 mx-auto">
		<c:if test="${param.accno ne null}">
			<c:choose>
				<c:when test="${found }">
					<table class="table table-bordered">
						<tr>
							<th>Account Number</th>
							<th>${account.accno }</th>
						</tr>
						<tr>
							<th>Customer Name</th>
							<th>${account.customer.cname }</th>
						</tr>
						<tr>
							<th>Gender</th>
							<th>${account.customer.gender }</th>
						</tr>
						<tr>
							<th>Account Type</th>
							<th>${account.actype }</th>
						</tr>
						<tr class="table-success">
							<th>Balance Available</th>
							<th>&#8377; ${account.bal }</th>
						</tr>
					</table>
					<c:choose>
					<c:when test="${account.isactive == 'Yes' }">
					<div class="alert alert-success text-center">
						<strong>Account is already active</strong>
					</div>
					</c:when>
					<c:otherwise>
					<form method="post" action="/reactivate"
						onsubmit="return confirm('Are you sure to activate this account ?')">
						<input type="hidden" name="accno"
							value="${param.accno }">
						<button class="btn btn-success">Reactivate Account</button>
					</form>
					</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<div class="alert alert-danger text-center">
						<strong>Invalid Account Number</strong>
					</div>
				</c:otherwise>
			</c:choose>
		</c:if>

	</div>
</div>
<jsp:include page="footer.jsp" />