package com.bank.controllers;

import java.sql.Date;
import java.time.LocalDate;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bank.model.Account;
import com.bank.model.Beneficiary;
import com.bank.model.NBUser;
import com.bank.model.Transactions;
import com.bank.model.Users;
import com.bank.service.AccountService;
import com.bank.service.BeneficiaryService;
import com.bank.service.CustomerService;
import com.bank.service.TransactionService;
import com.bank.service.UserService;

@Controller
public class NetBankingController {
	
	@Autowired UserService userservice;
	@Autowired CustomerService custservice;
	@Autowired TransactionService transervice;
	@Autowired HttpSession session;
	@Autowired AccountService accservice;
	@Autowired BeneficiaryService bservice;
	
	@GetMapping("/netbanking")
	public String Home() {
		return "NetBanking";
	}
	
	@PostMapping("/NBRegister")
	public String Register(NBUser user) {
		int cid=accservice.validateDetails(user);
		if(cid!=-1) {
			userservice.AddUser(new Users(user.getUserid(), user.getUname(), user.getPwd(), cid));
			session.setAttribute("msg", "Registeration successfull");
		}
		else {
			session.setAttribute("error", "Invalid details provided");
		}
		return "redirect:/netbanking";
	}
	
	@PostMapping("/nbvalidate")
	public String Login(String userid,String pwd) {
		Users user=userservice.login(userid, pwd);
		if(user!=null) {
			session.setAttribute("userid", userid);
			session.setAttribute("uname", user.getUname());
			session.setAttribute("cid", user.getCid());
			return  "redirect:/nbhome";
		}else {
			session.setAttribute("msg", "Invalid userid and password");
			return "redirect:/netbanking";
		}
	}
	
	@GetMapping("/nbhome")
	public String UserHome(Model model) {
		int cid=Integer.parseInt(session.getAttribute("cid").toString());
		Account account=accservice.findByCustomerId(cid);
		model.addAttribute("account", account);
		return "CustomerHome";
	}
	
	@PostMapping("/CheckName")
	@ResponseBody
	public String CheckName(int accno) {
		Account account=accservice.findByAccno(accno);
		if(account==null) {
			return "no";
		}else {
			return account.getCustomer().getCname();
		}
	}
	
	@GetMapping("/onlinetransfer")
	public String OnlineTransfer() {
		return "OnlineTransfer";
	}
	
	@GetMapping("/nbchange")
	public String ChangePassword() {
		return "NBChangePwd";
	}
	
	@PostMapping("/nbchange")
	public String ChangePassword(Users user,String opwd){
		if(userservice.ValidatePwd(user,opwd)) {
			userservice.ChangePwd(user);
			session.setAttribute("msg", "Password updated");
		}else {
			session.setAttribute("error", "Invalid current password");
		}
		return "redirect:/nbchange";
	}
	
	@GetMapping("/SameBank")
	public String SameBank(Model model) {
		int cid=Integer.parseInt(session.getAttribute("cid").toString());
		Account account=accservice.findByCustomerId(cid);
		model.addAttribute("account", account);		
		return "SameBank";
	}
	
	@PostMapping("/SameBank")
	public String SameBankEntry(Transactions tran,int recaccno) {
		tran.setTdate(Date.valueOf(LocalDate.now()));		
		transervice.transfer(tran, recaccno);		
		session.setAttribute("msg", "Transfer successfully");
		return "redirect:/SameBank";
	}
	
	@GetMapping("/CustomerReport")
	public String Report(Model model) {
		int cid=Integer.parseInt(session.getAttribute("cid").toString());
		Account account=accservice.findByCustomerId(cid);
		model.addAttribute("account", account);
		model.addAttribute("trans", transervice.allAccountTransactions(account.getAccno()));
		return "CustomerReport";
	}
	
	@GetMapping("/OtherBank")
	public String OtherBank(Model model) {
		int cid=Integer.parseInt(session.getAttribute("cid").toString());
		Account account=accservice.findByCustomerId(cid);
		model.addAttribute("account", account);
		model.addAttribute("list", bservice.findByCustomerId(cid));
		return "OtherBank";
	}
	
	@PostMapping("/OtherBank")
	public String SaveEntry(Transactions tran) {
		tran.setTdate(Date.valueOf(LocalDate.now()));		
		transervice.withdraw(tran);		
		session.setAttribute("msg", "Transfer successfully");
		return "redirect:/OtherBank";
	}
	
	@GetMapping("/Beneficiary")
	public String beneficiary(Model model) {
		int cid=Integer.parseInt(session.getAttribute("cid").toString());
		model.addAttribute("list", bservice.findByCustomerId(cid));
		return "Beneficiary";
	}
	
	@PostMapping("/Beneficiary")
	public String addBeneficiary(Beneficiary b) {
		bservice.saveBeneficiary(b);
		session.setAttribute("msg", "Beneficiary saved");
		return "redirect:/Beneficiary";
	}
}
