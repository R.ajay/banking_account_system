package com.bank.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.model.Account;

@Repository
public interface AccountDAO extends JpaRepository<Account,Integer> {
	
	@Query("SELECT max(accno)+1 FROM Account")
	int generateAccountNo();
	
	@Query("SELECT p FROM Account p WHERE cid=?1")
	Account findByCustomerId(int cid);
}
