package com.bank.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.dao.AccountDAO;
import com.bank.dao.CustomerDAO;
import com.bank.dao.TransactionDAO;
import com.bank.model.Account;
import com.bank.model.NBUser;
import com.bank.model.Transactions;

@Service
public class AccountService {
	@Autowired AccountDAO dao;
	@Autowired TransactionDAO tdao;
	@Autowired CustomerDAO cdao;
	
	public List<Account> allAccounts() {
		return dao.findAll();
	}
	
	public int validateDetails(NBUser user) {
		Account account=findByAccno(user.getAccno());
		if(account!=null) {
			if(user.getAdhar().equals(account.getCustomer().getAdhar()) 
					&& user.getPhone().equals(account.getCustomer().getPhone())) {
				return account.getCid();
			}
			else
				return -1;
		}
		return -1;
	}
	
	public Account findByAccno(int accno) {
		Optional<Account> account= dao.findById(accno);
		if(account.isPresent() && account.get().getIsactive().equals("Yes"))
			return dao.findById(accno).get();
		else
			return null;
	}
	
	public Account findDeactiateByAccno(int accno) {
		Optional<Account> account= dao.findById(accno);
		if(account.isPresent())
			return dao.findById(accno).get();
		else
			return null;
	}
	
	public void activate(int accno) {
		Account account= dao.findById(accno).get();
		account.setIsactive("Yes");
		dao.save(account);
	}
	
	public void deactivate(int accno) {
		Account account= dao.findById(accno).get();
		account.setIsactive("No");
		dao.save(account);
	}
	
	public void openAccount(Account acc,int bal) {
		acc.setIsactive("Yes");	
		acc.setAccno(generateAccno());
		acc.setCustomer(cdao.findById(acc.getCid()).get());
		dao.save(acc);
		tdao.save(new Transactions(Date.valueOf(LocalDate.now()), "Account Open", acc.getAccno(), 0, bal));
	}
	
	public int generateAccno() {
		return dao.count()==0 ? 10001 : dao.generateAccountNo();
	}
	
	public Account findByCustomerId(int cid) {
		return dao.findByCustomerId(cid);
	}
}
