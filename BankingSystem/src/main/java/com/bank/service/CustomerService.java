package com.bank.service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bank.dao.CustomerDAO;
import com.bank.model.Customer;

@Service
public class CustomerService {
	
	@Autowired CustomerDAO dao;
	@Autowired ServletContext ctx;
	
	public void addCustomer(Customer c,MultipartFile photo,MultipartFile sign) {
		final String picname=generatePhotoName();
		final String signname=generateSignName();
		save(photo, "photo", picname);
		save(sign, "sign", signname);
		c.setPic(picname);
		c.setSignature(signname);
		dao.save(c);
	}
	
	public void updateCustomer(Customer c) {
		Customer cc=getCustomerById(c.getId());
		c.setPic(cc.getPic());
		c.setSignature(cc.getSignature());
		dao.save(c);
	}
	
	public List<Customer> getAllCustomers(){
		return dao.findAll();
	}
	
	public List<Customer> getPendingCustomers(){
		return dao.findPendingCustomer();
	}
	
	public Customer getCustomerById(int id) {
		return dao.findById(id).get();
	}
	
	public String generatePhotoName() {
		return "Pic"+(dao.count()+1)+".jpg";
	}
	
	public String generateSignName() {
		return "Sign"+(dao.count()+1)+".jpg";
	}
	
	public void save(MultipartFile file,String type,String filename) {
		try {
				String folder=type.equals("photo") ? "/cpics/":"/csign/";			
			
		      Files.copy(file.getInputStream(),Paths.get(ctx.getRealPath(folder),filename),StandardCopyOption.REPLACE_EXISTING);
		    } catch (Exception e) {
		      throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
		    }
	}
}
